
mongoose = require "mongoose"
getTags = (tags) ->
  return tags.join ','

setTags = (tags) ->
  return tags.split ','


problemSchema = mongoose.Schema
  title : 
  	type: String
  	trim: true
  	required : true
  body : String
  resource : String
  tags : String
  difficulty: Number,
  createdAt :
  	type: Date
  	default: Date.now
  bnuOjId  :
  	type: String
  	trim: true
  	required: true
  tags :
  	type: []



module.exports = mongoose.model "Problem" , problemSchema
