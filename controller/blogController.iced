mongoose = require 'mongoose'
marked   = require 'marked'
_ = require 'underscore'


Blog = mongoose.model 'Blog'

exports.showCreateForm = (req, res)->
	res.render 'blog/showCreateForm',
		blog: new Blog({})
	return

exports.create = (req, res)->
	blog = new Blog req.body
	problem.save (err) ->
		if err
			console.log err
			res.render 'blog/showCreateForm',
				errors : err.errors,
				blog : blog
		else
			req.session.message = "Blog Created Successfully"
			res.redirect '/blogs/'
		return
	return

exports.showEditForm = (req, res)->
	blog = req.blog
	res.render 'blog/showCreateForm',
		blog : blog
	return

exports.update = (req, res)->
	blog = req.blog
	blog = _.extend blog, req.body
	blog.save (err)->
		if err
			res.render 'blog/showCreateForm',
				errors: err.errors,
				blog: blog
		else
			req.session.message = "Blog updated Successfully"
			res.redirect '/blogs/'+blog.id+'/show'
		return
	return


exports.show = (req, res)->
	blog = req.blog
	res.render 'blog/show',
		blog: blog
	return


export.showFeatured = (req, res)->
	Problem.find({'featured': true}).exec (err, blogs) ->
		res.render 'blog/index',
	      blogs : blogs
	    return


exports.blog = (req, res, next, id) ->
  Blog.findById(id).exec (err, blog) ->
    return next err if err
    return next new Error 'Failed to load Blog' if not blog
      
    req.blog = blog
    next()
    return
  return

