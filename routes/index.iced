

admin = require "./admin"
user = require "./user"
vjudge = require "./vjudge"
problem = require "./problem"

@bind = (app)->

    # Route Groups

    app.get "/" , (req , res)->
        res.status(200).render "index"

    app.use "/user/" , user.router
    app.use "/vjudge/" , vjudge.router
    app.use "/admin/" , admin.router
    app.use "/problems/", problem.router
