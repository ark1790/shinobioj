mongoose = require 'mongoose'
marked   = require 'marked'
_ = require 'underscore'




Problem = mongoose.model 'Problem'

@index = (req, res)->
	Problem.find({}).sort({ difficulty : 'asc'}).exec (err, problems) ->
		res.render 'admin/problems/index',
	      problems : problems
	    return

@showCreateForm = (req, res)->
	res.render 'admin/problems/showCreateForm',
		problem: new Problem({})
	return


@create = (req, res)->
	problem = new Problem req.body
	problem.save (err) ->
		if err
			console.log err
			res.render 'admin/problems/showCreateForm',
				errors : err.errors,
				problem : problem
		else
			req.session.message = "Problem Created Successfully"
			res.redirect '/admin/problems'
		return
	return


@showEditForm = (req, res)->
	problem = req.problem
	res.render 'admin/problems/showCreateForm',
		problem:problem
	return

@update = (req, res)->
	problem = req.problem
	problem = _.extend problem, req.body
	problem.save (err) ->
	    if err
	      res.render 'admin/problems/showCreateForm',
	        problem: problem
	        errors: err.errors
	    else
	      req.flash 'notice', problem.title + ' was successfully updated.'
	      res.redirect '/admin/problems/'+problem.id+'/show'
	    return
	return

@show = (req, res)->
	problem = req.problem
	problem.body = marked(problem.body)
	problem.resource = marked(problem.resource)
	res.render 'admin/problems/show',
		problem : problem
	return

@indexNormalUser = (req, res)->
	Problem.find({}).sort({ difficulty : 'asc'}).exec (err, problems) ->
		res.render 'problems/index',
	      problems : problems
	    return

@showNormalUser = (req, res)->
	if not req.problem
		return res.send "NO SUCH PROBLEM"

	problem = req.problem
	problem.body = marked(problem.body)
	problem.resource = marked(problem.resource)
	res.render 'problems/show',
		problem : problem
	return

@showDeleteForm = (req,res)->
	problem = req.problem
	res.render 'admin/problems/deleteForm',
		problem : problem
	return


@destroy = (req, res) ->
	problem = req.problem
	problem.remove (err) ->
		req.flash 'notice', problem.title + ' was successfully deleted.'
		res.redirect '/admin/problems'
	return

# exports.problem = (req, res, next, id) ->
#   console.log id
#   Problem.findById(id).exec (err, problem) ->
#     return next err if err
#     return next new Error 'Failed to load Problem' if not problem
#
#     req.problem = problem
#     next()
#     return
#   return

@submit = (req , res)->
 	problemId = req.query.pid
 	authorId = req.user.id
 	res.send {pid : problemId , aid : authorId}
 	return
