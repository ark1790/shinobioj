

mongoose = require "mongoose"
bcrypt = require "bcrypt-nodejs"


userSchema = mongoose.Schema
	
	role			: String

	local			: 
		email		: String
		password	: String

	solved		 	: []


# METHODS

# generating a hash 

userSchema.methods.generateHash = ( password , next ) ->
	bcrypt.hash password , bcrypt.genSaltSync(8) , null , next


# Checking if a password is valid

userSchema.methods.validPassword  = ( password , next ) ->
	bcrypt.compare password , this.local.password , next


# Create the model for users and expose it to out app

module.exports = mongoose.model "User" , userSchema