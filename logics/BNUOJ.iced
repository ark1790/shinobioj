
request = require "request"
request = request.defaults {jar: true}

mongoose = require "mongoose"

Submission = mongoose.model "Submission"

username = "armankamal" # change
password = "secret" # change

data = 
	"user_id" : username 
	"problem_id" : "1001" 
	"language" : "1"
	"isshare" : "0" 
	"source" : "FEDERER" 
	"login" : "submit"



loginData = 
	"username" : username
	"password" : password


loginURL = "http://www.bnuoj.com/v3/ajax/login.php"
submitURL = "http://www.bnuoj.com/v3/ajax/problem_submit.php"
statusURL = "http://www.bnuoj.com/v3/ajax/status_data.php?sEcho=2&iColumns=10&sColumns&iDisplayStart=0&iDisplayLength=20&mDataProp_0=0&mDataProp_1=1&mDataProp_2=2&mDataProp_3=3&mDataProp_4=4&mDataProp_5=5&mDataProp_6=6&mDataProp_7=7&mDataProp_8=8&mDataProp_9=9&sSearch&bRegex=false&sSearch_0="+loginData['username']+"&bRegex_0=false&bSearchable_0=true&sSearch_1&bRegex_1=false&bSearchable_1=true&sSearch_2&bRegex_2=false&bSearchable_2=true&sSearch_3&bRegex_3=false&bSearchable_3=true&sSearch_4&bRegex_4=false&bSearchable_4=true&sSearch_5&bRegex_5=false&bSearchable_5=true&sSearch_6&bRegex_6=false&bSearchable_6=true&sSearch_7&bRegex_7=false&bSearchable_7=true&sSearch_8&bRegex_8=false&bSearchable_8=true&sSearch_9&bRegex_9=false&bSearchable_9=true&iSortCol_0=1&sSortDir_0=desc&iSortingCols=1&bSortable_0=false&bSortable_1=false&bSortable_2=false&bSortable_3=false&bSortable_4=false&bSortable_5=false&bSortable_6=false&bSortable_7=false&bSortable_8=false&bSortable_9=false&_=1438066613419"

updateUrl = "http://www.bnuoj.com/v3/ajax/get_source.php?runid="


login = (credentials , next)->
	await request.post {url : loginURL , formData : credentials } , defer err , resp , body
	
	if err?
		console.log err
		return next err
	next()


submitCode = (submitData , next)->
	await request.post {url : submitURL, formData : submitData} , defer err , resp 
	
	if err?
		console.log err
		return next err

	next()


getStatus = ( next ) ->
	await request.get {url : statusURL} , defer err , resp , body

	if err?
		console.log err
		return next err
		

	body = JSON.parse body
	
	next(null , body.aaData)

updateLatest = (problem_id , code , next)->
	getStatus (err , data)->
		submission = new Submission()
		submission.oj = "bnuoj"
		submission.code = code
		submission.problem_id = problem_id
		submission.language = "cpp"
		submission.verdict = data[0][3]
		submission.runID = data[0][1]

		submission.save (err)->
			return next(null , submission)

updateBySubId = (subID , next)->
	Submission.findById subID , (error , submission)->
		if error?
			console.log error
			return next(error)

		await request.get { url : updateUrl + submission.runID } , defer err , resp , body

		if err?
			console.log err
			return next(err)
		
		body = JSON.parse body
		submission.verdict = body.result

		submission.save (err1)->
			if err1?
				console.log err1
				next err1
			next(null , submission)


exports.updateBySubId = updateBySubId

exports.submit = (problem_id , code , next)->
	submitData = data
	submitData.problem_id = problem_id

	submitData.source = code
	login loginData , (err)->
		submitCode submitData , (err)->
			updateLatest problem_id , code, (err , submission)->
				next(null , submission)
