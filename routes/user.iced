express = require "express"
@router = express.Router()
passport = require "passport"


@controller = require "../controller/userController"
@middleware = require("../middleware/user").bind



@router.get "/login", @controller.getLogin

@router.post "/login" , passport.authenticate "local-login" , {
	successRedirect : "/user/profile"
	failureRedirect : "/user/login"
	failureFlash : true
}

@router.get "/signup" , @controller.getSignUp

@router.post "/signup" , passport.authenticate "local-signup" , {
	successRedirect : "/user/profile"
	failureRedirect : "/user/signup"
	failureFlash : true
}


@router.route( "/profile" )
	.all(@middleware.loggedIn)
	.get(@controller.getProfile)

@router.route('/logout')
	.all(@middleware.loggedIn)
	.get @controller.getLogout
