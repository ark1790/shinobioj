

@bind = 
    loggedIn : (req , res , next) ->
        if not req.isAuthenticated()
            req.flash "error" , "Login required!"
            return res.redirect "/login"
        next()

    isAdmin : (req , res , next) ->
        if not req.isAuthenticated()
            req.flash "error" , "Login required!"
            return res.redirect "/login"

        {user} = req

        if user.role is not 'admin'
            req.flash "error" , "Access Denied!"
            return res.redirect "/login"

        next()
