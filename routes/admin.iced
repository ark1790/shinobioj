express = require "express"
@router  = express.Router()
problemController = require "../controller/problemController"

user = require "./user"


# router.use "*" , user.middleware.isAdmin

@router.get "/", (req, res)->
	res.render "admin/index"


@router.get "/problems", problemController.index
@router.get "/problems/create", problemController.showCreateForm
@router.post "/problems/create", problemController.create
@router.get  "/problems/:problemId/edit", problemController.showEditForm
@router.post "/problems/:problemId/edit", problemController.update
@router.get "/problems/:problemId/show", problemController.show
@router.get "/problems/:problemId/destroy", problemController.showDeleteForm
@router.post "/problems/:problemId/destroy", problemController.destroy

# @router.param 'problemId', problemController.problem
