

BNUOJController = require "./OJ/BNUOJController"
Submission = require "../models/submission"
request = require "request"

module.exports = 

	index : (req,res)->
		res.render "vjudge/index"

	
	submit: (req , res)->
		if req.body? && (req.body.oj is "bnuoj")
			BNUOJController.submit req , res

	status: (req , res ) ->
		Submission.findById  req.params.id , (err , sub)->
			if err?
				return res.err err
				
			if sub.verdict in ["Judging" , "Submitted"]
				BNUOJController.updateStatus sub.id , (err , submission)->
					if err?
						console.log err
						return res.send err
					res.render "vjudge/run_status" , submission
			else 
				res.render "vjudge/run_status" , sub
