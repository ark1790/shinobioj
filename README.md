# Shinobi Online Judge


1. Download the source
2. Install by using the command `npm install`
3. Create a mongoDB instance and update the configuration at `config/database`
4. Use `npm start` to start the server.
5. Use `nodemon -x iced app.iced` to start `nodemon`