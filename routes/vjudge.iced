
express = require "express"
@router = express.Router()

@controller = require "../controller/vjudgeController"

@router.get "/" , @controller.index

@router.post "/problem-submit" , @controller.submit

@router.get "/status/:id" , @controller.status
