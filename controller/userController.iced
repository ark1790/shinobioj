
@getIndex = (req , res) ->
	res.render "index"

@getLogin = (req , res) ->
	res.render "login" , message : req.flash "loginMessage"

@getSignUp = (req , res) ->
	res.render "signup" , message : req.flash "signupMessage"

@getProfile = (req , res)->
	res.render "profile" , user : req.user

@getLogout = (req , res)->
	req.logout()
	res.redirect "/"
