
mongoose = require "mongoose"
Problem = mongoose.model "Problem"

@fetchOne = (req , res , next)->
    { problemId } = req.params

    await Problem.findById(problemId).exec  defer err , problem
    if err?
        req.error = err
        return next()

    if not problem
        req.error = new Error "Failed to load problem"
        return next()

    req.problem = problem
    
    return next()
