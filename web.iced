
express = require "express"
app = express()
mongoose = require "mongoose"
passport = require "passport"
flash = require "connect-flash"

morgan = require "morgan"
cookieParser = require "cookie-parser"
bodyParser = require "body-parser"
session = require "express-session"
path = require "path"




require('dotenv').load()


#configDB = require "./config/database"

app.locals = require "./config/constants"


# REQUIRING ALL MODELS IN THE APP

fs  = require "fs"
models_path = __dirname + "/models/"

fs.readdirSync(models_path).forEach (file)->
	require models_path + file

# configuration

mongoose.connect process.env.MONGO_URL



require("./config/passport")(passport)

# setup express application

app.use morgan('dev')
app.use cookieParser()
app.use bodyParser.json()
app.use bodyParser.urlencoded
	extended : false

app.use express.static( path.join( __dirname, "public"))
app.use express.static( path.join( __dirname, "assets"))
app.use require('connect-assets')()


app.set 'view engine' , 'jade'


# REDIS for session storage

# store = new (require('connect-redis') session)
#   url : process.env.REDISCLOUD_URL
#   ttl: 24*3600
app.use session
	secret : process.env.SECRET
	resave : false
	saveUninitialized: true
	# store: store
	# key: 's.id'


# passport settings

app.use passport.initialize()
app.use passport.session()
app.use flash()




require("./routes").bind app



app.set 'port' , process.env.PORT || "9090"


server = app.listen app.get('port') , ->
	console.log 'listening on 9090'


module.exports = app
