BNUOJ = require "../../logics/BNUOJ"
Submission = require "../../models/submission"

exports.submit = (req , res) ->
	BNUOJ.submit req.body.problem_id , req.body.code  , (err , data)->
			if err?
				console.log err
				return res.send err
			res.redirect "/vjudge/status/" + data.id

exports.updateStatus = (subID , next)->

	Submission.findById  subID , (err , sub)->
		if err?
			return next err

		if sub.verdict in ["Judging" , "Submitted"]
			BNUOJ.updateBySubId sub.id , (err , submission)->
				if err?
					console.log err
					return next err
				next null , submission
		else
			next null , sub
