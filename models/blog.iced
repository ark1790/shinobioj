mongoose = require "mongoose"

blogSchema = mongoose.Schema
	title : 
	  	type: String
	  	trim: true
	  	required : true
  	body : String
  	featured: 
  		type: Boolean
  		default: false
  	createdAt :
	  	type: Date
	  	default: Date.now


module.exports = mongoose.model "Blog" , blogSchema