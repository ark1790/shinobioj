express = require "express"
@router  = express.Router()
@controller = require "../controller/problemController"
@middleware = require "../middleware/problems"


user = require "./user"



@router.route("/")
    .get @controller.indexNormalUser

# the following portion is buggy
@router.route( "/submit" )
    .all(user.middleware.loggedIn)
    .get(@controller.submit )

@router.route("/:problemId")
    .all(@middleware.fetchOne)
    .get @controller.showNormalUser
